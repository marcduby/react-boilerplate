// @flow

import * as React from 'react'
import {
    Link
} from 'react-router-dom'
import firebase from '../firebase/index.js'

import 'semantic-ui/dist/semantic.min.css'
import './style.css'

type Props = {
    count: number
}

type State = {
    count: number,
    userEnteredValue: boolean,
    checkboxIsChecked: boolean
}

class BlankComponent extends React.Component<Props, State> {
    state = {
        count: this.props.match.params.count,
        userEnteredValue: false,
        checkboxIsChecked: false
    }

    constructor (props) {
        super(props)
        this.onUserInput = this.onUserInput.bind(this)
        this.onClickCheckbox= this.onClickCheckbox.bind(this)
        this.handleSubmit= this.handleSubmit.bind(this)
    }

    handleSubmit (event) {
        // event.preventDefault()
        const itemsRef = firebase.database().ref('items')
        const item = {
            count: this.state.userValue
        }
        itemsRef.push(item)
        this.setState({
            currentCount: ''
        })
    }

    onUserInput (event) {
        let newCount = event.currentTarget.value
        if (!newCount.isNan && newCount !== '') {
            this.setState({
                userEnteredValue: true,
                userValue: newCount
            })
        } else {
            this.setState({
                userEnteredValue: false,
                userValue: 'undef'
            })
        }
    }

    onClickCheckbox () {
        this.setState({
            checkboxIsChecked: !this.state.checkboxIsChecked
        })
    }

    render() {
        return (
            <div>
                {this.props.match.params.count !== 'undef' &&
                    <div>
                        <h3 className="ui header">Your choice: {this.props.match.params.count} (wow, you changed the URL!)</h3>
                    </div>
                }
                <div className="ui vertical center aligned segment">
                    <div className="ui center form">
                        <div className="ui inline field">
                            <div className="ui input">
                                <input
                                    type="number"
                                    placeholder="Change count..."
                                    onInput={this.onUserInput}
                                />
                            </div>
                        </div>
                        <div className="ui field">
                            <div className={`ui toggle checkbox`}>
                                <input
                                    placeholder="Count.."
                                    type="checkbox"
                                    name="terms"
                                    className="count user input"
                                    onClick={this.onClickCheckbox}
                                    checked={this.state.checkboxIsChecked}
                                />
                                <label>Accept terms and conditions</label>
                            </div>
                        </div>
                        <div className="ui field">
                            <Link
                                to={`/blank/${this.state.userValue}`}
                                style={this.state.userEnteredValue && this.state.checkboxIsChecked ? null : {pointerEvents: "none"}}
                            >
                                <button
                                    className={`ui button ${this.state.userEnteredValue && this.state.checkboxIsChecked ? '' : 'disabled'}`}
                                >
                                    Change
                                </button>
                            </Link>
                            <Link
                                to={`/blank/undef`}
                                style={this.state.userEnteredValue && this.state.checkboxIsChecked ? null : {pointerEvents: "none"}}
                            >
                                <button
                                    onClick={this.handleSubmit}
                                    className={`ui button ${this.state.userEnteredValue && this.state.checkboxIsChecked ? '' : 'disabled'}`}
                                >
                                    Add
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default BlankComponent