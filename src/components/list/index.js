// @flow

import * as React from 'react'
import firebase from '../firebase/index.js'

import 'semantic-ui/dist/semantic.min.css'
import './style.css'

type Props = {
    count: number
}

type State = {
    count: number,
    userEnteredValue: boolean,
    checkboxIsChecked: boolean
}

class ListComponent extends React.Component<Props, State> {
    state = {
        items: []
    }

    constructor (props) {
        super(props)
        this.removeItem = this.removeItem.bind(this)
    }

    removeItem (itemId) {
        const itemRef = firebase.database().ref(`/items/${itemId}`)
        itemRef.remove()
    }

    componentWillMount () {
        const itemsRef = firebase.database().ref('items')
        itemsRef.on('value', (snapshot) => {
            let items = snapshot.val()
            let newState = []
            for (let item in items) {
                newState.push({
                    id: item,
                    count: items[item].count
                })
            }
            this.setState({
                items: newState
            })
        })
    }

    render() {
        return (
            <div className="ui vertical segment">
                <div className="ui relaxed horizontal divided list">
                    {this.state.items.map((item, i) => {
                       return(
                            <div className="item" key={`item-${i}`}>
                                <div className="content">
                                    <i
                                        className="ui red icon remove"
                                        onClick={() =>this.removeItem(item.id)}
                                    />
                                    {item.count}
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default ListComponent